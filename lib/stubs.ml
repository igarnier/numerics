open Bigarray

type float64_vec = (float, Bigarray.float64_elt, Bigarray.c_layout) Array1.t

type float64_mat = (float, Bigarray.float64_elt, Bigarray.c_layout) Array2.t

type complex64_vec =
  (Complex.t, Bigarray.complex64_elt, Bigarray.c_layout) Array1.t

type complex64_mat =
  (Complex.t, Bigarray.complex64_elt, Bigarray.c_layout) Array2.t

(* Pointwise addition *)
external float64_vec_add :
  dst:float64_vec -> src1:float64_vec -> src2:float64_vec -> unit
  = "caml_float64_vec_add_"

external float64_mat_add :
  dst:float64_mat -> src1:float64_mat -> src2:float64_mat -> unit
  = "caml_float64_vec_add_"

external complex64_vec_add :
  dst:complex64_vec -> src1:complex64_vec -> src2:complex64_vec -> unit
  = "caml_complex64_vec_add_"

external complex64_mat_add :
  dst:complex64_mat -> src1:complex64_mat -> src2:complex64_mat -> unit
  = "caml_complex64_vec_add_"

(* Pointwise subtraction *)
external float64_vec_sub :
  dst:float64_vec -> src1:float64_vec -> src2:float64_vec -> unit
  = "caml_float64_vec_sub_"

external float64_mat_sub :
  dst:float64_mat -> src1:float64_mat -> src2:float64_mat -> unit
  = "caml_float64_vec_sub_"

external complex64_vec_sub :
  dst:complex64_vec -> src1:complex64_vec -> src2:complex64_vec -> unit
  = "caml_complex64_vec_sub_"

external complex64_mat_sub :
  dst:complex64_mat -> src1:complex64_mat -> src2:complex64_mat -> unit
  = "caml_complex64_vec_sub_"

(* Pointwise multiplication *)
external float64_vec_mul :
  dst:float64_vec -> src1:float64_vec -> src2:float64_vec -> unit
  = "caml_float64_vec_mul_"

external float64_mat_mul :
  dst:float64_mat -> src1:float64_mat -> src2:float64_mat -> unit
  = "caml_float64_vec_mul_"

external complex64_vec_mul :
  dst:complex64_vec -> src1:complex64_vec -> src2:complex64_vec -> unit
  = "caml_complex64_vec_mul_"

external complex64_mat_mul :
  dst:complex64_mat -> src1:complex64_mat -> src2:complex64_mat -> unit
  = "caml_complex64_vec_mul_"

(* Division by scalar *)
external float64_vec_div_scalar : v:float64_vec -> x:float -> unit
  = "caml_float64_vec_div_scalar_"

external complex64_vec_div_scalar : v:complex64_vec -> x:float -> unit
  = "caml_complex64_vec_div_scalar_"

(* total sum *)
external float64_vec_sum : v:float64_vec -> float = "caml_float64_vec_sum_"

external float64_mat_sum : v:float64_mat -> float = "caml_float64_vec_sum_"

external complex64_vec_sum : v:complex64_vec -> Complex.t
  = "caml_complex64_vec_sum_"

external complex64_mat_sum : v:complex64_mat -> Complex.t
  = "caml_complex64_vec_sum_"

(* pointwise pow *)
external float64_vec_pow : p:float -> dst:float64_vec -> src:float64_vec -> unit
  = "caml_float64_vec_pow_"

external float64_mat_pow : p:float -> dst:float64_mat -> src:float64_mat -> unit
  = "caml_float64_vec_pow_"

external complex64_vec_pow :
  p:Complex.t -> dst:complex64_vec -> src:complex64_vec -> unit
  = "caml_complex64_vec_pow_"

external complex64_mat_pow :
  p:Complex.t -> dst:complex64_mat -> src:complex64_mat -> unit
  = "caml_complex64_vec_pow_"

(* forward fft *)
external owl_float64_rfftf : float64_vec -> complex64_vec -> int -> unit
  = "float64_rfftf"

(* backward fft *)
external owl_float64_rfftb : complex64_vec -> float64_vec -> int -> unit
  = "float64_rfftb"
