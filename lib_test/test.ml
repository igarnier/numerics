open Numerics

let pp_floatvec fmtr vec =
  let a = Float64.Vec.to_array vec in
  let l = Array.to_list a in
  Format.pp_print_list
    ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "; ")
    (fun fmtr float -> Format.fprintf fmtr "%.3f" float)
  fmtr
  l

let get_row mat i =
  let _lines, cols = Float64.Mat.shape mat in
  Float64.Vec.init cols (fun j -> Float64.Mat.get mat i j)

let pp_floatmat fmtr mat =
  let lines, _cols = Float64.Mat.shape mat in
  let rows = Array.init lines (fun i -> get_row mat i) in
  Array.iteri (fun i row ->
    Format.fprintf fmtr "%d: %a@;" i pp_floatvec row
    ) rows

let pp_complexvec fmtr vec =
  let a = Complex64.Vec.to_array vec in
  let l = Array.to_list a in
  Format.pp_print_list
    ~pp_sep:(fun fmtr () -> Format.fprintf fmtr "; ")
    (fun fmtr c -> Format.fprintf fmtr "%.3f + %.3fi" c.Complex.re c.Complex.im)
  fmtr
  l

module Fvec =
struct
  module V = Float64.Vec

  let v1 = V.init 10 (fun i -> float i)
  let v2 = V.init 10 (fun i -> float i)

  let res = V.add v1 v2

  let () =
    Format.printf "add: %a@." pp_floatvec res

  let res = V.sub v1 v2

  let () =
    Format.printf "sub: %a@." pp_floatvec res

  let res = V.mul v1 v2

  let () =
    Format.printf "mul: %a@." pp_floatvec res

  let res = V.linspace 0.0 10.0 10

  let () =
    Format.printf "lin: %a@." pp_floatvec res

  let res = V.pow 2. v1

  let () =
    Format.printf "pow 2.: %a@." pp_floatvec res

  let res = V.sum v1

  let () =
    Format.printf "sum: %f@." res
end


module Fmat =
struct
  module V = Float64.Mat

  let v1 = V.init ~lines:10 ~cols:10 ~f:(fun i j -> float (i + j))
  let v2 = V.init ~lines:10 ~cols:10 ~f:(fun i j -> float (i - j))

  let res = V.add v1 v2

  let () =
    Format.printf "add:@.%a@." pp_floatmat res

  let res = V.sub v1 v2

  let () =
    Format.printf "sub:@.%a@." pp_floatmat res

  let res = V.mul v1 v2

  let () =
    Format.printf "mul: %a@." pp_floatmat res

  let res = V.pow 2. v1

  let () =
    Format.printf "pow 2.: %a@." pp_floatmat res

  let res = V.sum v1

  let () =
    Format.printf "sum: %f@." res
end

module Cvec =
struct
  module V = Complex64.Vec

  let v1 = V.init 10 (fun i -> { Complex.re = float i ; im = float i })
  let v2 = V.init 10 (fun i -> { Complex.re = float i ; im = float i })

  let res = V.add v1 v2

  let () =
    Format.printf "%a@." pp_complexvec res

  let res = V.sub v1 v2

  let () =
    Format.printf "sub:@.%a@." pp_complexvec res

  let res = V.mul v1 v2

  let () =
    Format.printf "%a@." pp_complexvec res


  let res = V.linspace Complex.{ re = 0.0; im = 0.0 } Complex.{ re = 10.0; im = 0.0 } 10

  let () =
    Format.printf "lin: %a@." pp_complexvec res

  let res = V.pow Complex.{ re = 2.; im = 0.0 } v1

  let () =
    Format.printf "pow 2.: %a@." pp_complexvec res

  let res = V.sum v1

  let () =
    Format.printf "sum: %f + %fi@." res.re res.im

end
